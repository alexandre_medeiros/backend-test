import Controller from '../controllers';

export default (app) => {

  const controller = new Controller();

  app.route('/')
    .post((req, res) => {

      controller.checkPalindrome(req.body).then(response => {
          res.status(response.statusCode);
          res.json(response.data);
      }).catch(error => {
          res.status(error.statusCode);
          res.json(error.data);
      });
    });

}
