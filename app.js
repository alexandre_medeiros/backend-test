import express from 'express';
import bodyParser from 'body-parser';

import Router from './routes';

const app = express();

app.set('port', 7000);
app.use(bodyParser.json());

Router(app);

export default app;
