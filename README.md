# Backend Challenge

A palindrome word checker

# Install Dependencies

  - run npm install

# Run API

   - npm run start
   - Open your browser with http://localhost:7000

# Run test
   
   - npm run test