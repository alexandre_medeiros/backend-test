const defaultResponse = (data, statusCode = 200) => ({
	data,
	statusCode
});

const errorResponse = (message, statusCode = 400) => defaultResponse({
	error: message,
}, statusCode);

class Controller {

    checkPalindrome(data) {
		return new Promise((fulfill, reject) => {
			let word = data.word;
			let noWhite = word.replace(/\s/g,'');

			let isPalindrome = (noWhite == noWhite.split('').reverse().join(''));

			if ( isPalindrome ) {
				fulfill( defaultResponse({success: 'IS PALINDROME'}, 200) );
			} else {
				reject(errorResponse('NOT IS PALINDROME', 400));
			}
		});
	}
}

export default Controller;